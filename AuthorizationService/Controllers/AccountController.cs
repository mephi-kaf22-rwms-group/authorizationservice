﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using AuthorizationService.Models;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using AuthorizationService.ViewModels;
using Newtonsoft.Json.Linq;
using System;
using Microsoft.Data.SqlClient;

namespace AuthorizationService.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        private string GetUserName(string name, string surname)
        {
            return name + surname;
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult Register()
        {
            return View();
        }
        
        [HttpPost]
        [Authorize(Roles= "Administrator")]
        public async Task<IActionResult> Register(RegisterViewModel registerViewModel)
        {
            if (ModelState.IsValid)
            {
                string userName = GetUserName(registerViewModel.Name, registerViewModel.Surname);
                User user = new User { UserName = userName,
                                       Name = registerViewModel.Name,
                                       Surname = registerViewModel.Surname };
                var identityResult = await _userManager.CreateAsync(user, registerViewModel.Password);
                if (identityResult.Succeeded)
                {
                    await (registerViewModel.UserType == "Student" ?
                           _userManager.AddToRoleAsync(user, "Student") :
                           _userManager.AddToRoleAsync(user, "Teacher"));
                    return Redirect("/Account/Register");
                }
                else
                {
                    foreach (var error in identityResult.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }
            }
            return View(registerViewModel);
        }

        private string GetToken()
        {
            JObject tokenSettings = JObject.Parse(System.IO.File.ReadAllText("TokenSettings.json"));
            string token = "";
            Random random = new Random((int)(DateTimeOffset.Now.ToUnixTimeSeconds() % 1000000));
            for (int index = 1; index <= int.Parse((string)tokenSettings["tokenLength"]); index++)
            {
                token += ((string)tokenSettings["characters"])[random.Next(0, ((string)tokenSettings["characters"]).Length - 1)];
            }
            string expirationTime = (DateTimeOffset.Now.ToUnixTimeSeconds() + int.Parse((string)tokenSettings["validityPeriod"])).ToString();
            using (SqlConnection sqlConnection = new SqlConnection((string)tokenSettings["connectionString"]))
            {
                sqlConnection.Open();
                string sqlExpression = "INSERT INTO dbo.Tokens " +
                                       "(Token, ExpirationTime) " +
                                       "VALUES ('" + token + "', " + expirationTime + ");";
                SqlCommand sqlCommand = new SqlCommand(sqlExpression, sqlConnection);
                sqlCommand.ExecuteNonQuery();
            }
            return token;
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                var signInResult = await _signInManager.PasswordSignInAsync(loginViewModel.UserName, loginViewModel.Password, loginViewModel.Remember, false);
                if (signInResult.Succeeded)
                {
                    JObject administratorAccount = JObject.Parse(System.IO.File.ReadAllText("AdministratorAccount.json"));
                    if (loginViewModel.UserName != (string)administratorAccount["userName"])
                    {
                        await _signInManager.SignOutAsync();
                        return Redirect(loginViewModel.ReturnUrl + "?token=" + GetToken());
                    }
                    return Redirect("/Account/Register");
                }
                else
                {
                    ModelState.AddModelError("", "Неверные логин и (или) пароль");
                }
            }
            return View(loginViewModel);
        }

        [HttpGet]
        public IActionResult IsTokenValid(string token = null)
        {
            if (token == null)
            {
                return StatusCode(400);
            }
            JObject tokenSettings = JObject.Parse(System.IO.File.ReadAllText("TokenSettings.json"));
            using (SqlConnection sqlConnection = new SqlConnection((string)tokenSettings["connectionString"]))
            {
                sqlConnection.Open();
                string sqlExpression = "SELECT COUNT(*) " +
                                       "FROM dbo.Tokens " +
                                       "WHERE Token = '" + token + "' " +
                                       "AND CAST(DATEDIFF(SECOND, '1970-01-01 00:00:00', GETUTCDATE()) AS BIGINT) < ExpirationTime;";
                SqlCommand sqlCommand = new SqlCommand(sqlExpression, sqlConnection);
                if ((int)sqlCommand.ExecuteScalar() == 0)
                {
                    return StatusCode(403);
                }
            }
            return StatusCode(200);
        }

        [HttpGet]
        public async Task<IActionResult> Logout(string token = null)
        {
            JObject administratorAccount = JObject.Parse(System.IO.File.ReadAllText("AdministratorAccount.json"));
            if (!User.IsInRole((string)administratorAccount["role"]))
            {
                if (token == null)
                {
                    return StatusCode(400);
                }
                JObject tokenSettings = JObject.Parse(System.IO.File.ReadAllText("TokenSettings.json"));
                string expirationTime = DateTimeOffset.Now.ToUnixTimeSeconds().ToString();
                using (SqlConnection sqlConnection = new SqlConnection((string)tokenSettings["connectionString"]))
                {
                    sqlConnection.Open();
                    string sqlExpression = "UPDATE dbo.Tokens " +
                                           "SET ExpirationTime = " + expirationTime + " " +
                                           "WHERE token = '" + token + "';";
                    SqlCommand sqlCommand = new SqlCommand(sqlExpression, sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                }
                return StatusCode(200);
            }
            await _signInManager.SignOutAsync();
            return Redirect("/Account/Login");
        }
    }
}
