﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using AuthorizationService.Models;
using Newtonsoft.Json.Linq;

namespace AuthorizationService
{
    public class Initializer
    {
        public static async Task Initialize(RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            if (await roleManager.FindByNameAsync("Administrator") == null)
            {
                await roleManager.CreateAsync(new Role { Name = "Administrator" });
            }
            if (await roleManager.FindByNameAsync("Student") == null)
            {
                await roleManager.CreateAsync(new Role { Name = "Student" });
            }
            if (await roleManager.FindByNameAsync("Teacher") == null)
            {
                await roleManager.CreateAsync(new Role { Name = "Teacher" });
            }
            JObject administratorAccount = JObject.Parse(System.IO.File.ReadAllText("AdministratorAccount.json"));
            if (await userManager.FindByNameAsync((string)administratorAccount["userName"]) == null)
            {
                User administrator = new User { UserName = (string)administratorAccount["userName"] };
                var identityResult = await userManager.CreateAsync(administrator, (string)administratorAccount["password"]);
                if (identityResult.Succeeded)
                {
                    await userManager.AddToRoleAsync(administrator, (string)administratorAccount["role"]);
                }
            }
        }
    }
}
