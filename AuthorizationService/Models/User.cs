﻿using Microsoft.AspNetCore.Identity;

namespace AuthorizationService.Models
{
    public class User : IdentityUser
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
