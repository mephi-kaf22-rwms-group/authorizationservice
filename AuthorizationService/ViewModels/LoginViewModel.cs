﻿using System.ComponentModel.DataAnnotations;

namespace AuthorizationService.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить")]
        public bool Remember { get; set; }

        public string ReturnUrl { get; set; }
    }
}
