﻿using Microsoft.AspNetCore.Mvc;
using AuthorizationServiceCommunication;
using System.IO;
using System.Net;

namespace ClientForAuthorizationService.Controllers
{
    public class AccountController : Controller
    {
        private const string AuthorizationServiceUrl = "https://localhost:5001";
        private const string ClientForAuthorizationServiceUrlForAuthorized = "https://localhost:5003/Home/ForAuthorized";
        private const string ClientForAuthorizationServiceUrlForUnauthorized = "https://localhost:5003/Home/ForUnauthorized";

        private readonly UrlManager _urlManager;

        public AccountController()
        {
            _urlManager = new UrlManager();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return Redirect(
                _urlManager.GetLoginUrl(
                    AuthorizationServiceUrl,
                    ClientForAuthorizationServiceUrlForAuthorized
                )
            );
        }
        
        [HttpGet]
        public IActionResult Logout()
        {
            string token;
            using (StreamReader streamReader = new StreamReader("Token.txt", System.Text.Encoding.Default))
            {
                token = streamReader.ReadLine();
            }
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(_urlManager.GetLogoutUrl(AuthorizationServiceUrl, token));
                using (HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    return httpWebResponse.StatusCode == HttpStatusCode.OK ?
                        Redirect("/Home/ForUnauthorized") :
                        Redirect("/Home/ForAuthorized");
                }
            }
            catch (WebException webException)
            {
                return Redirect("/Home/ForAuthorized");
            }
        }
    }
}
