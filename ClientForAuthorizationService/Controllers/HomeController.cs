﻿using Microsoft.AspNetCore.Mvc;
using AuthorizationServiceCommunication;
using System.IO;

namespace ClientForAuthorizationService.Controllers
{
    public class HomeController : Controller
    {
        private const string AuthorizationServiceUrl = "https://localhost:5001";
        
        private readonly TokenManager _tokenManager;

        public HomeController()
        {
            _tokenManager = new TokenManager();
        }

        public IActionResult ForUnauthorized()
        {
            return View();
        }

        public IActionResult ForAuthorized(string token = null)
        {
            if (token == null)
            {
                return StatusCode(400);
            }
            using (StreamWriter streamWriter = new StreamWriter("Token.txt", false, System.Text.Encoding.Default))
            {
                streamWriter.WriteLine(token);
            }
            string token_;
            using (StreamReader streamReader = new StreamReader("Token.txt", System.Text.Encoding.Default))
            {
                token_ = streamReader.ReadLine();
            }
            if (!_tokenManager.IsTokenValid(AuthorizationServiceUrl, token))
            {
                return Redirect("/Home/ForUnauthorized");
            }
            return View();
        }
    }
}
