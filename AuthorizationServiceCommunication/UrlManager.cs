﻿namespace AuthorizationServiceCommunication
{
    public class UrlManager
    {
        public string GetLoginUrl(string authorizationServiceUrl, string returnUrl)
        {
            return authorizationServiceUrl + "/Account/Login?returnUrl=" + returnUrl;
        }

        public string GetLogoutUrl(string authorizationServiceUrl, string token)
        {
            return authorizationServiceUrl + "/Account/Logout?token=" + token;
        }
    }
}
