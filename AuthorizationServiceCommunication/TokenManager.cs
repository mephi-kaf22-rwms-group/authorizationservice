﻿using System.Net;

namespace AuthorizationServiceCommunication
{
    public class TokenManager
    {
        public bool IsTokenValid(string authorizationServiceUrl, string token)
        {
            try
            {
                HttpWebRequest httpWebRequest =
                    (HttpWebRequest)WebRequest.Create(authorizationServiceUrl + "/Account/IsTokenValid?token=" + token);
                using (HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    return httpWebResponse.StatusCode == HttpStatusCode.OK;
                }
            }
            catch (WebException webException)
            {
                return false;
            }
        }
    }
}
